@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Manage Patrols</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">All Patrols DataTable</h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
      <div class="col-sm-12 col-md-6">
        <div id="dataTable_filter" class="dataTables_filter">
          <a href="{{ url('/add-patrol') }}" class="btn btn-primary btn-icon-split" style="float: right; margin-right:20px; margin-top:20px">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Add Site Patrol</span>
          </a>
      </div>
    </div>
        </div>

    
      <div class="card-body">
        @if (session('update'))
        <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> {{ session('update') }} </strong>
        </div>
        @endif
        <div class="table-responsive">
          
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>                
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Comments</th>
                <th>Actions</th>
              </tr>
            </thead>
            
            <tbody>

              @foreach($data as $i)
               <tr>
                <td>{{$i->id}}</td>
                <td>{{$i->name}}</td>                
                <td>{{$i->date}}</td>
                <td>{{$i->starttime}}</td>
                <td>{{$i->endtime}}</td>
                <td>{{$i->comment}}</td>
                <td>
                <!-- Card Header - Dropdown -->
          <div class="">
            <div class="dropdown no-arrow">
              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <form action="patrol-information" method="POST" >
                  <input type="hidden" value="{{$i->id}}" name="id">
                  @csrf
                <button class="dropdown-item btn btn-info btn-circle btn-sm" type="submit"><i class="fas fa fa-eye"></i></button>View
                </form>
                
                <div class="dropdown-divider"></div>
                <form action="deletepatrol" method="POST">
                  <input type="hidden" value="{{$i->id}}" name="id">
                  @csrf
                <button class="dropdown-item btn btn-danger btn-circle btn-sm" type="submit"><i class="fas fa-trash"></i></button>Delete
                </form>
              </div>
            </div>
          </div>
                  
                  </td>
              </tr>
              @endforeach

             
            </tbody>
          </table>
        </div>
      </div>
    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
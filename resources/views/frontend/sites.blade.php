@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Manage Sites</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">All Sites DataTable</h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter">
                <a href="{{ url('/add-site') }}" class="btn btn-primary btn-icon-split" style="float: right; margin-right:20px">
                  <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                  </span>
                  <span class="text">Add New Site</span>
                </a>
            </div>
          </div>
        </div>

    
      <div class="card-body">
        @if (session('update'))
        <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> {{ session('update') }} </strong>
        </div>
        @endif
        <div class="table-responsive">
          
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Site ID</th>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>Site Manager</th>
                <th>Contact No</th>
                <th>Instructions</th>
                <th>Actions</th>
              </tr>
            </thead>
            
            <tbody>

              @foreach($data as $i)
               <tr>
                <td>{{$i->id}}</td>
                <td>{{$i->name}}</td>
                <td>{{$i->address}}</td>
                <td>{{$i->city}}</td>
                <td>{{$i->manager}}</td>
                <td>{{$i->contact}}</td>
                <td>{{$i->comment}}</td>
                <td>
                  <a href="{{ url('/edituser') }}" class="btn btn-info btn-circle btn-sm">
                    <i class="fas fa-edit"></i>
                  </a>Edit
                  
                  <form action="deletesite" method="POST">
                  <input type="hidden" value="{{$i->id}}" name="id">
                  @csrf
                <button class="btn btn-danger btn-circle btn-sm" type="submit"><i class="fas fa-trash"></i></button>Delete
                </form>
                  </td>
              </tr>
              @endforeach

             
            </tbody>
          </table>
        </div>
      </div>
    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
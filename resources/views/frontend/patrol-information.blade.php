@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">View Patrol Details</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter">
                
            </div>
          </div>
        </div>

    
      <div class="card-body">
        @foreach($data as $i)
          

        <h6 class="heading-small text-muted mb-4">Google Map Location </h6>
        <div class="row">
       <div class="col">
         <div class="card border-0">
       
         <iframe src = "https://maps.google.com/maps?q={{$i->lati}},{{$i->longi}}&hl=es;z=14&amp;output=embed" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>					
              </div>
       </div>
       </div>

          <form action="form-post.php" method="post" enctype="multipart/form-data">
            <h6 class="heading-small text-muted mb-4"></h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Officer Name</label>
        <input class="form-control" type="text" value="{{$i->name}}" name="name" readonly="">	                        
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Date</label>
                    <input class="form-control" type="text" value="{{$i->date}}" name="date" readonly="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Start Time</label>
                            <input class="form-control" type="text" value="{{$i->starttime}}" name="starttime" readonly=""> 

                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-last-name">End Time</label>
                            <input class="form-control" type="text" value="{{$i->endtime}}" name="endtime" readonly="">

                  </div>
                </div>
              </div>
            </div>
    
    <h6 class="heading-small text-muted mb-4">Location</h6>
            <div class="pl-lg-4">
              <div class="form-group">
               
      <input type="text" class="form-control" id="demo" name="demo" value="{{$i->lati}}" readonly="">
      <input type="text" class="form-control" id="demo1" name="demo1" value="{{$i->longi}}" readonly="">
      
              </div>
            </div>				
    
            <hr class="my-4">
            <!-- Address -->
            <h6 class="heading-small text-muted mb-4">Check  Points</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-md-12 container1">
                  <div class="form-group">
                    <label class="form-control-label" for="input-address">Check Point 1</label>
                    <a class="dropdown-item" href="image-information.php?id=image.jpg">							
        <img src="https://proforce.udigisolutions.com/uploads/15919697058661501154846621994495.jpg" id="the-img" width="100px" class="image"><br>
         <p id="pic-info">This photo was taken on undefined with a undefined undefined</p>
         <script src="{{ asset('public/js/exif.js') }}"></script>
         <script type="text/javascript">
            document.getElementById("the-img").onload = function() {
              EXIF.getData(this, function() {
                myData = this;
                console.log(myData.exifdata);
                document.getElementById('pic-info').innerHTML = 'This photo was taken on ' + myData.exifdata.DateTime + ' with a ' + myData.exifdata.Make + ' ' + myData.exifdata.Model;
                 });
            }

          </script>
        
                    </a>	
                                         
                  </div>					
                </div>
      
      <hr>			
      
      </div>                  
            </div>
    <hr class="my-4">
     <div class="pl-lg-4">
              <div class="row">
      
                <div class="col-md-12 container1">
                  <div class="form-group">
                    <label class="form-control-label" for="input-address">Check Point 2</label>
                    <a class="dropdown-item" href="image-information.php?id=image.jpg">							
        <img src="https://proforce.udigisolutions.com/uploads/15919697058661501154846621994495.jpg" id="the-img1" width="100px" class="image"><br>
         <p id="pic-info1">This photo was taken on undefined with a undefined undefined</p>
         <script type="text/javascript">
            document.getElementById("the-img1").onload = function() {
              EXIF.getData(this, function() {
                myData = this;
                console.log(myData.exifdata);
                document.getElementById('pic-info1').innerHTML = 'This photo was taken on ' + myData.exifdata.DateTime + ' with a ' + myData.exifdata.Make + ' ' + myData.exifdata.Model;
                 });
            }

          </script>
        
                    </a>	
                                         
                  </div>					
                </div>
      
      <hr>			
      
      </div>                  
            </div>
      
            <hr class="my-4">				
    <h6 class="heading-small text-muted mb-4">Any Issues While on Patrol</h6>
    <div class="col-md-12">
    <div class="form-group">
    <label class="custom-toggle">
            <input type="checkbox" name="issue" checked="">
            <span class="custom-toggle-slider rounded-circle" data-label-off="NO" data-label-on="YES"></span>
            </label>
    </div>
    </div>
     <hr class="my-4">
            <!-- Description -->
            <h6 class="heading-small text-muted mb-4">Additional Comments</h6>
            <div class="pl-lg-4">
              <div class="form-group">
                <label class="form-control-label">Comment</label>
                <textarea rows="4" class="form-control" name="comment" placeholder="" readonly="">{{$i->comment}}</textarea>
              </div>
            </div>
    </form>
    @endforeach

     
      <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
        <div class="d-flex justify-content-between">
          
          <a href="{{ url('/patrols') }}" class="btn btn-sm btn-default float-left">Back</a>
  <button class="btn btn-sm btn-default float-right" onclick="window.print()">Print this page</button>
  
        </div>
      </div>
    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
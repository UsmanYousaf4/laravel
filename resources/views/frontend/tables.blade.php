@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">User DataTable</h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter">
                <a href="{{ url('/register') }}" class="btn btn-primary btn-icon-split" style="float: right; margin-right:20px">
                  <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                  </span>
                  <span class="text">Add New User</span>
                </a>
            </div>
          </div>
        </div>

    
      <div class="card-body">
        <div class="table-responsive">
          
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created Date</th>
                <th>Last Updated</th>
                <th>Actions</th>
              </tr>
            </thead>
            
            <tbody>

              @foreach($data as $i)
               <tr>
                <td>{{$i->id}}</td>
                <td>{{$i->name}}</td>
                <td>{{$i->email}}</td>
                <td>{{$i->created_at}}</td>
                <td>{{$i->updated_at}}</td>
                <td>
                  <a href="{{ url('/edituser') }}" class="btn btn-info btn-circle btn-sm">
                    <i class="fas fa-edit"></i>
                  </a>Edit
                  
                  <form action="delete" method="POST">
                  <input type="hidden" value="{{$i->id}}" name="id">
                  @csrf
                <button class="btn btn-danger btn-circle btn-sm" type="submit"><i class="fas fa-trash"></i></button>Delete
                </form>
                  </td>
              </tr>
              @endforeach

             
            </tbody>
          </table>
        </div>
      </div>
    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Site Patrol Form</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter">
                
            </div>
          </div>
        </div>

    
      <div class="card-body">
        <h6 class="heading-small text-muted mb-4">Please Tap the Location button Before starting a Site Patrol </h6>
				<div class="col-md-12">
				<div class="form-group">
				<button class="btn btn-sm btn-info  mr-4" onclick="getLocation()">Get My Location</button>							
				</div>
				</div>
				
				<div class="col-md-12">
				<div class="form-group">
				<h6 class="heading-small text-muted mb-4">Location</h6>
				<p id="demo"></p>	
				</div>
				</div>
         
        <form method="POST" action="submitpatrol" enctype="multipart/form-data">
          @csrf

          <h6 class="heading-small text-muted mb-4">Patrol Information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Officer Username</label>
                        <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}" readonly>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Date</label>
                      <input class="form-control" type="text" value="{{ $ldate = date('Y-m-d') }}"  name="date" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Start Time</label>
                      <input class="form-control" type="text" value="{{ $ltime = date('H:i:s') }}" name="starttime" readonly> 

                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">End Time</label>
                                <input class="form-control" type="text" value="Get Automatically" name="endtime" required>

                      </div>
                    </div>
                  </div>
                </div>
				
                <div class="pl-lg-4">
                  <div class="form-group">
                   
					<input type="hidden" class="form-control" id="lati" name="lati" value="" >
					<input type="hidden" class="form-control" id="longi" name="longi" value="" >	
					
					<script>
					var x = document.getElementById("demo");
                    var y = document.getElementById("lati");
					var z = document.getElementById("longi"); 
					 
					 
					function getLocation() {
					  if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(showPosition, showError);
					  } else { 
						x.innerHTML = "Geolocation is not supported by this browser.";
					  }
					}

					function showPosition(position) {
					  x.innerHTML = "<p class='alert alert-success'>Latitude: " + position.coords.latitude + 
					  "<br>Longitude: " + position.coords.longitude + "<p>";
					  
					  var lati = position.coords.latitude;
					  var longi = position.coords.longitude;
						
					  y.value =  lati;
					  z.value =  longi;
					}

					function showError(error) {
					  switch(error.code) {
						case error.PERMISSION_DENIED:
						  x.innerHTML = "<p class='alert alert-danger'>User denied the request for Geolocation.<p>"
						  break;
						case error.POSITION_UNAVAILABLE:
						  x.innerHTML = "Location information is unavailable."
						  break;
						case error.TIMEOUT:
						  x.innerHTML = "The request to get user location timed out."
						  break;
						case error.UNKNOWN_ERROR:
						  x.innerHTML = "An unknown error occurred."
						  break;
					  }
					}     
                    
					</script>
                  </div>
                </div>			
				
				
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Check  Points</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Check Point 1</label>
						<input type="file" class="form-control" placeholder="" name="filep"  accept="image/*" capture="camera" required>			
                      </div>
                    </div>
					<div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Check Point 2</label>
						<input type="file" class="form-control" placeholder="" name="filep1"  accept="image/*" capture="camera" required>			
                      </div>
                    </div>
					
					</div>
                  
                </div>
                <hr class="my-4" />
				
				<h6 class="heading-small text-muted mb-4">Any Issues While on Patrol</h6>
				<div class="col-md-12">
				<div class="form-group">
				<label class="custom-toggle">
                <input type="checkbox" name="issue" checked>
                <span class="custom-toggle-slider rounded-circle" data-label-off="NO" data-label-on="YES"></span>
                </label>
				
				</div>
				</div>
				 <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Additional Comments</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Comment</label>
                    <textarea rows="4" class="form-control" name="comment" placeholder="" required></textarea>
                  </div>
                </div>
			   </div>
				
				
				<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
			    <a href="dashboard.php" class="btn btn-sm btn-default float-left">Back</a>
                <button type="submit" name="submit" class="btn btn-sm btn-info  mr-4">Send</button>
                
              </div>
            </div>


  </form>

    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
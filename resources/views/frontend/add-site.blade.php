@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add New Site</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>

      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div id="dataTable_filter" class="dataTables_filter">
         
        </div>
      </div>
        
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter">
                
            </div>
          </div>
        </div>

    
      <div class="card-body">
         
        <form method="POST" action="submit" enctype="multipart/form-data">
          @csrf        
          <h6 class="heading-small text-muted mb-4">Site Information</h6>
           <div class="pl-lg-4">
             <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-username">Site Name</label>
       <input class="form-control" type="text" value=""  name="name" required>	                        
                 </div>
               </div>                   
             </div>
     
             <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">Address</label>
                           <input class="form-control" type="text" value="" name="address" required> 

                 </div>
               </div>                    
             </div>
     
     <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">City</label>
                           <input class="form-control" type="text" value="" name="city" required> 

                 </div>
               </div>                    
             </div>

   <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">Post code</label>
                    <input class="form-control" type="text" value="" name="pc" required> 

                 </div>
               </div>                    
             </div>
     
     <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">Location</label>
                    <input class="form-control" type="text" value="" name="lati" placeholder="Latitude" required> <br>
        <input class="form-control" type="text" value="" name="longi" placeholder="Longitude" required> 

                 </div>
               </div>                    
             </div>
           
   <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">Site Manager</label>
                           <input class="form-control" type="text" value="" name="manager" required> 

                 </div>
               </div>                    
             </div>

            <div class="row">
               <div class="col-lg-6">
                 <div class="form-group">
                   <label class="form-control-label" for="input-first-name">Contact Information</label>
                           <input class="form-control" type="text" value="" name="contact" required> 

                 </div>
               </div>                    
             </div>
     
           </div>
   
   
                  
    <hr class="my-4" />
           <!-- Description -->
           <h6 class="heading-small text-muted mb-4">Instructions</h6>
           <div class="pl-lg-4">
             <div class="form-group">
               <label class="form-control-label"></label>
               <textarea rows="4" class="form-control" name="comment" placeholder=""></textarea>
             </div>
           </div>
   
   <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
         <div class="d-flex justify-content-between">
           
           <a href="sites.php" class="btn btn-sm btn-default float-left">Back</a>
           <button type="submit" name="submit" class="btn btn-sm btn-info  mr-4">Send</button>
   
         </div>
       </div>
  </form>

    </div>

    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection
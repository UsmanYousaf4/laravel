<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', function () {
    return view('auth/register');
});

Route::get('/', function () {
    return view('frontend/login');
});

Route::get('/buttons', function () {
    return view('frontend/buttons');
});

Route::get('/cards', function () {
    return view('frontend/cards');
});

Route::get('/reports', function () {
    return view('frontend/reports');
});

Route::get('/login', function () {
    return view('frontend/login');
});

Route::get('/edituser', function () {
    return view('frontend/edituser');
});

Route::get('/add-site','AddsiteController@view')->name('add-site')->middleware('auth');

Route::get('/add-patrol', function () {
    return view('frontend/add-patrol');
});

Route::post('submitpatrol','AddpatrolController@addpatrols');

Route::post('submit','AddsiteController@addsites');

Route::get('/sites','SitesController@list')->name('sites')->middleware('auth');
Route::post('/deletesite','AddsiteController@deletesite');

Route::get('/patrols','PatrolController@list')->name('patrols')->middleware('auth');
Route::post('/deletepatrol','PatrolController@deletepatrol');
Route::post('/patrol-information','PatrolController@viewpatrol')->middleware('auth');

Route::get('/logbook','BookController@list')->name('logbook')->middleware('auth');
Route::post('/deletebook','BookController@deletebook');


Route::get('/tables','Users@list', 'HomeController@tables')->middleware('auth');

Route::post('/delete','Deleteusers@delete');

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin')->middleware('auth');







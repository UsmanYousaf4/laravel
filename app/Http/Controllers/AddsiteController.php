<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addsite;
class AddsiteController extends Controller
{
    //
    function addsites (Request $req) 
    {
      //print_r($req->input());
      $site = new Addsite;
      $site->name = $req->name;
      $site->address = $req->address;
      $site->pc = $req->pc;
      $site->city = $req->city;
      $site->manager = $req->manager;
      $site->contact = $req->contact;
      $site->longi = $req->longi;
      $site->lati = $req->lati;
      $site->comment = $req->comment;
      $site->save();
      return redirect()->route('sites')->with('update', 'Site Record has been Added successfully!'); 
    }

    function deletesite(Request $req) {
        //print_r($req->input());
          $deletesite=Addsite::find($req->id);
          $deletesite->delete();
          return redirect()->route('sites')->with('update', 'Record has been Deleted successfully!');
      } 
      
      function view() {
         return view('frontend/add-site');
        }   
}

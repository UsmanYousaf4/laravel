<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patrol;
class PatrolController extends Controller
{
    //
    function list() {
        $data= Patrol::all();
        return view('frontend/patrols',['data'=>$data]);
        }

    function viewpatrol(Request $id) {
        $data= Patrol::find($id);
        return view('frontend/patrol-information',['data'=>$data]);        
         }

    function deletepatrol(Request $req) {
      //print_r($req->input());
        $deleteuser=Patrol::find($req->id);
        $deleteuser->delete();        
        return redirect()->route('patrols')->with('update', 'Record has been Deleted successfully!');
    }     
}

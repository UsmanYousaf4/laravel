<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addpatrol;
class AddpatrolController extends Controller
{
    function addpatrols (Request $req) 
    {
      //print_r($req->input());
      
      $req->file('filep')->store('public/images');
      $req->file('filep1')->store('public/images');

      $pattol = new Addpatrol;
      $pattol->name = $req->name;
      $pattol->date = $req->date;
      $pattol->starttime = $req->starttime;
      $pattol->endtime = $req->endtime;
      $pattol->issue = $req->issue;
      $pattol->image = $req->filep;
      $pattol->image1 = $req->filep1;
      $pattol->longi = $req->longi;
      $pattol->lati = $req->lati;
      $pattol->comment = $req->comment;
      $pattol->save();
      return redirect()->route('patrols')->with('update', 'Site Record has been Added successfully!'); 
    }

    function view() {
      return view('frontend/add-patrol');
    }
}

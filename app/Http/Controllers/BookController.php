<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{
    //
    function list() {
        $data= Book::all();
        return view('frontend/logbook',['data'=>$data]);
        }

    function deletebook(Request $req) {
       //print_r($req->input());
       $deletbook=Book::find($req->id);
       $deletbook->delete();
       return redirect()->route('logbook')->with('update', 'Record has been Deleted successfully!');
        }
}

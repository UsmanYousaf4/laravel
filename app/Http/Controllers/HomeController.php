<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patrol;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data= Patrol::all();
        return view('frontend/index',['data'=>$data]);        
    }

    public function tables()
    {
        return view('frontend/tables');
    }
}

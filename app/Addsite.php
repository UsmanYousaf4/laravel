<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addsite extends Model
{
    //
    protected $table = 'sites';
    public $timestamps =false;
}
